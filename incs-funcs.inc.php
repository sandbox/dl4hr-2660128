<?php

/**
 * @file
 * Header to correctly include the other headers.
 *
 * This file is one of the main reasons we are able to combine WP and
 * Drupal code. We check, based on the functions defined, and then
 * include the correct bootstrapping header files.
 */

require_once __DIR__ . "/includes/lang.inc.php";
require_once __DIR__ . "/includes/crypt.inc.php";
/**
 * Check the CMS identity and do includes.
 */
function sedamicro_get_cms() {

  $expected_wp_load_file = "../../../wp-load.php";
  if (file_exists($expected_wp_load_file)) {
    include_once $expected_wp_load_file;
    include_once ABSPATH . 'wp-admin/includes/plugin.php';
    include_once ABSPATH . 'wp-admin/includes/media.php';
    include_once ABSPATH . 'wp-admin/includes/file.php';
    include_once ABSPATH . 'wp-admin/includes/image.php';
    include_once ABSPATH . 'wp-admin/includes/plugin.php';
    include_once __DIR__ . "/includes/wp_interface.inc.php";
    if (!function_exists('wp_handle_upload')) {
      include_once ABSPATH . 'wp-admin/includes/file.php';
    }
    if (!is_plugin_active("sedamicro/seda-micro.php")) {
      die("Plugin is disabled");
    }
    $cms = new SedaMicroWPInterface();
  }
  else {
    if (!defined('DRUPAL_ROOT')) {
      define('DRUPAL_ROOT', dirname(dirname(dirname(dirname(__DIR__)))));
    }
    include_once __DIR__ . "/includes/dl_interface.inc.php";
    $cms = new SedaMicroDLInterface();
    include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    if (!module_exists("sedamicro") && !defined('NO_AUTH')) {
      die("Module is disabled");
    }
  }
  return $cms;
}
sedamicro_get_cms();
