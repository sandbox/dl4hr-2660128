<?php
/**
 * @file
 * Plugin Name: SedaMicro Uploader.
 *
 * Plugin URI: https://github.com/icthr/sedamicro
 * Description: This plugin will add allow file upload directly from the
 * Android app to the Amazon S3 if correctly connected.
 * Version: 0.7.0
 * Author: ICT4HR
 * Author URI: http://www.ict4hr.net/
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html.
 *
 * @package SedaMicro
 *
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License
 *
 * @link http://www.ict4hr.net
 */

require_once dirname(__File__) . '/includes/seda-micro-option.inc.php';
require_once dirname(__File__) . '/includes/crypt.inc.php';

/**
 * The main class that is used by Wordpress.
 */
class SedaMicro {
  /*
   *  Construct
   *
   *  @description:
   *  @since: 3.6
   *  @created: 1/04/15
   */
  public $sedakeys = 0;
  /**
   * Default constructor.
   *
   * The idea is to generate the keys, add menu upon installation.
   */
  public function __construct() {

    // Set text domain
    // version 3-.
    add_action('admin_menu', 'seda_micro_menu');
    $sedakeys = new SedaCrypt();
    if (!get_option("sedakeys")) {
      $key_arr = $sedakeys->generate();
      if ($key_arr['error'] == 0) {
        add_option("sedakeys", $key_arr, 0, 0);
      }
      else {
        die("error generating keys");
      }
    }
  }


  /**
   * Init.
   *
   * @description:
   *
   * @since: 3.6
   *
   * @created: 1/04/15
   */
  public function init() {

    if (function_exists('register_field')) {
      register_field('seda-micro', dirname(__File__) . '/seda-micro.php');
    }
  }

  /**
   * Register_fields.
   *
   * @description:
   *
   * @since: 3.6
   *
   * @created: 1/04/15
   */
  public function register_fields() {

    include_once 'basic_file.php';
  }
  /**
   * Install.
   *
   * @description: check dependencies
   *
   * @since: 3.6
   *
   * @created: 9/1/2015
   */
  public function install() {

    if (!class_exists(Imagick)) {
      $error_message = __('This plugin requires <a href="http://php.net/manual/en/imagick.setup.php">Imagick</a> to be active!', 'Imagick');
      die($error_message);
    }

  }

}
new seda_micro();
