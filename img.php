<?php
/**
 * @file
 * Image creating function.
 *
 * This file is used to show the spam prevention scrambled
 * text on user's mobile/browser.
 *
 * @category Image Generator File
 *
 * @package SedaMicro
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 *
 * @link http://www.ict4hr.net
 */

require_once _DIR__ . "/incs-funcs.inc.php";
$cms = sedamicro_get_cms();
$eq = htmlspecialchars($_GET['eq']);
if (!isset($eq) || (strlen($eq) > 255) || (strlen($eq) < 16)) {
  die();
}

$c = new SedaMicroSedaCrypt();
$key = $cms->getOption("seda_keys");
$crypted = $c->c2a($eq, "", $key['symmetric']);
if (!empty($crypted)) {
  $c->str2img($crypted);
}
else {
  $c->str2img("Error!");
}
